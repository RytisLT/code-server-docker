FROM tccr.io/truecharts/code-server:v4.3.0
USER root
RUN apt update && apt install wget -y
RUN wget https://packages.microsoft.com/config/ubuntu/21.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
RUN dpkg -i packages-microsoft-prod.deb
RUN rm packages-microsoft-prod.deb
RUN code-server --install-extension redhat.vscode-yaml && \
    code-server --install-extension foxundermoon.shell-format && \
    code-server --install-extension ms-dotnettools.vscode-dotnet-runtime
USER coder
